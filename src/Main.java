import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.initiate();
    }

    private void initiate() {
        runJgitPractice();
    }

    private void runJgitPractice(){
        try {
            String projectPath = "google/guava";
            CommitLister commitLister = new CommitLister(projectPath);
            commitLister.iterateAndProcessCommits();
            commitLister.commitDiffs();
            commitLister.printFixInducingCommits(projectPath.split("/")[1]);
        } catch (IOException | GitAPIException e) {
            e.printStackTrace();
        }
    }
}
