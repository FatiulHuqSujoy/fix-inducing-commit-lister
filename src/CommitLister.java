import org.eclipse.jgit.api.BlameCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.blame.BlameResult;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.Edit;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import sentimentProcessor.SentimentCalculator;

import java.io.*;
import java.util.*;

public class CommitLister {
    public static final String ANALYSIS_PROJECTS_DIR = "analysis_projects";
    public static final String RESULT_DIR = "fix_inducing_commits/";

    SentimentCalculator sentimentCalculator = new SentimentCalculator();

    private List<GHCommit> ghCommits;
    private List<RevCommit> revCommits;
    private Set<String> fixInducingCommitShas;
    private Map<String, GHCommit> commitIndex;
    private Git git;
    private Repository repository;

    public CommitLister(String projectPath) throws IOException, GitAPIException {
        ghCommits = new ArrayList<>();
        revCommits = new ArrayList<>();
        fixInducingCommitShas = new LinkedHashSet<>();
        commitIndex = new HashMap<>();
        openRepo(projectPath);
    }

    public void openRepo(String projectPath) throws IOException, GitAPIException {
        String projectName = projectPath.split("/")[1];

        String filePath = "../" + ANALYSIS_PROJECTS_DIR + "/" + projectName + "/";
        File index = new File(filePath);
        if (index.exists()) {
            git = Git.open(new File(filePath));
            git.checkout();
        }
        else {
            git = Git.cloneRepository()
                    .setURI( "https://github.com/" + projectPath + ".git" )
                    .setDirectory(new File(filePath))
                    .call();
        }

        repository = git.getRepository();
    }

    public void iterateAndProcessCommits() throws GitAPIException, IOException {
        int count = 0;
        Iterable<RevCommit> revCommits = git.log().all().call();
        for (RevCommit revCommit : revCommits) {
            processRevCommit(revCommit, count);
            count++;
        }

        System.out.println(count);
    }

    private void processRevCommit(RevCommit revCommit, int count) {
        revCommits.add(revCommit);
        System.out.println("\n" + count + ". LogCommit: " + revCommit + " " + revCommit.getName());
    }

    public void commitDiffs() throws IOException, GitAPIException {
        int count = 0;
        for(RevCommit commit : revCommits){
            if(!isFixing(commit.getFullMessage()))
                continue;

            System.out.println("\nCurrent commit sha " + commit.getName());
            if(commit.getParentCount()>0){
                ObjectId parentCommit = commit.getParent(0).getTree();
                ObjectId childCommit = commit.getTree();

                System.out.println("Diff between parent tree: " + parentCommit + " and current tree" + childCommit);

                diffWithFormatter(parentCommit, childCommit, commit);
            }
            count++;
            if(count==5)
                break;
        }
    }

    private boolean isFixing(String message) {
        if(message.contains("bug") || message.contains("fix") || message.contains("patch"))
            return true;
        return false;
    }

    private void diffWithFormatter(ObjectId parentCommit, ObjectId currentCommit, RevCommit commit) throws IOException, GitAPIException {
        DiffFormatter df = new DiffFormatter(System.out);
        df.setRepository(repository);
        df.setDiffComparator(RawTextComparator.DEFAULT);
        df.setDetectRenames(true);
        List<DiffEntry> diffs = df.scan(parentCommit, currentCommit);

        for (DiffEntry diff : diffs) {
            String filePath = diff.getNewPath();
            System.out.println("\tFile: " + filePath);
            if(!filePath.endsWith(".java"))
                continue;

            for (Edit edit : df.toFileHeader(diff).toEditList()) {
                if(edit.getType().equals(Edit.Type.INSERT))
                    continue;

                int start = edit.getBeginA();
                int finish = edit.getEndA();
                blameChange(start, finish, filePath, commit);
            }

        }
    }

    private void blameChange(int start, int finish, String filePath, RevCommit commit) throws GitAPIException, IOException {
        BlameCommand blamer = new BlameCommand(repository);
        blamer.setStartCommit(repository.resolve(commit.getName()));
        blamer.setFilePath(filePath);
        BlameResult blame = blamer.call();

        for (int i=start+1; i<=finish; i++) {
            try {
                RevCommit blameCommit = blame.getSourceCommit(i);
                fixInducingCommitShas.add(blameCommit.getName());
                System.out.println("\t\tLine: " + i + ": " + blameCommit);
            } catch (ArrayIndexOutOfBoundsException | NullPointerException e){
                continue;
            }
        }
    }

    public void printFixInducingCommits(String fileName){
        fileName = RESULT_DIR + fileName;

        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(fileName + ".txt", false));
            writer.append(getShaList());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getShaList() {
        String text = "";
        for(String sha: fixInducingCommitShas){
            text += sha + "\n";
        }
        return text;
    }
}
