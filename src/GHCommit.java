import com.jcabi.github.Commit;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GHCommit extends GHPullContent{
    private String sha;
    private boolean isFixInducing;

    public GHCommit(Commit.Smart commit) {
        body = getMessageFromCommit(commit);
        date = getDateFromCommit(commit);
    }

    public GHCommit(RevCommit commit) {
        body = commit.getFullMessage();
        date = commit.getAuthorIdent().getWhen();
        sha = commit.getName();
    }

    private Date getDateFromCommit(Commit.Smart commit) {
        Date date = new Date();
        try {
            String commitDate = commit.json().
                    getJsonObject("author").
                    getString("date").split("T")[0];
            date = new SimpleDateFormat("yyyy-MM-dd").parse(commitDate);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private String getMessageFromCommit(Commit.Smart commit) {
        String commitMessage = "";
        try {
            commitMessage = commit.json().getString("message");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return commitMessage;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public boolean isFixInducing() {
        return isFixInducing;
    }

    public void setFixInducing(boolean fixInducing) {
        isFixInducing = fixInducing;
    }
}
