import com.jcabi.github.PullComment;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GHPullComment extends GHPullContent {

    public GHPullComment(PullComment.Smart pullComment) {
        body = getMessageFromPullComment(pullComment);
        date = getDateFromPullComment(pullComment);
    }

    private Date getDateFromPullComment(PullComment.Smart pullComment) {
        Date date = new Date();
        try {
            String pullCommentDate = pullComment.json().
                    getString("created_at").split("T")[0];
            date = new SimpleDateFormat("yyyy-MM-dd").parse(pullCommentDate);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private String getMessageFromPullComment(PullComment.Smart pullComment) {
        String pullCommentMessage = "";
        try {
            pullCommentMessage = pullComment.json().getString("body");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pullCommentMessage;
    }
}
