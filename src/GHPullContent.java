import java.util.Date;

public class GHPullContent implements Comparable<GHPullContent>{
    protected String body, sentiResult;
    protected int sentiment=0;
    protected Date date;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getSentiment() {
        return sentiment;
    }

    public void setSentiment(int sentiment) {
        this.sentiment = sentiment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSentiResult() {
        return sentiResult;
    }

    public void setSentiResult(String sentiResult) {
        this.sentiResult = sentiResult;
    }

    @Override
    public int compareTo(GHPullContent o) {
        return date.compareTo(o.getDate());
    }
}
